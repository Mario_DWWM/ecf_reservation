const express = require('express')
const bodyParser = require('body-parser')
const fs = require('fs')
const cors = require('cors')
const app = express()
const port = 3000

app.use(cors())
app.use(bodyParser.json());

let data = require("./horaires.json")

app.get('/', (req, res) => {
    res.header("Content-Type", 'application/json')
    res.send(data)
})

app.post('/', (req, res) => {
    let toAdd = req.body
    let newData = { ...data }

    // Année existe
    if (newData.reserved[toAdd.year]) {
        // Mois existe
        if (newData.reserved[toAdd.year][toAdd.month]) {
            //Jour existe
            if (newData.reserved[toAdd.year][toAdd.month][toAdd.day]) {
                // Heure existe
                if (newData.reserved[toAdd.year][toAdd.month][toAdd.day][toAdd.hour]) {
                    return res.status(500).send({ error: "Ce créneau est déjà réservé." })
                } else {
                    newData.reserved[toAdd.year][toAdd.month][toAdd.day][toAdd.hour] = toAdd.client
                }
            } else {
                newData.reserved[toAdd.year][toAdd.month][toAdd.day] = { [toAdd.hour]: toAdd.client }
            }
        } else {
            newData.reserved[toAdd.year][toAdd.month] = { [toAdd.day]: { [toAdd.hour]: toAdd.client } }
        }
    } else {
        newData.reserved[toAdd.year] = { [toAdd.month]: { [toAdd.day]: { [toAdd.hour]: toAdd.client } } }
    }

    fs.writeFile('./horaires.json', JSON.stringify(newData), err => {
        res.status(200)

        if (err) {
            res.send(err)
        }

        res.send()
    })
})

app.post('/delete', (req, res) => {
    let newData = { ...data }

    delete newData.reserved[req.body.year][req.body.month][req.body.day][req.body.hour]

    fs.writeFile('./horaires.json', JSON.stringify(newData), err => {
        res.status(200)

        if (err) {
            res.send(err)
        }

        res.send()
    })
})

app.listen(port, () => console.log(`Serveur running at http://localhost:${port}`))