// ----------------------------------------------------------------------------
//      CONFIG
// ----------------------------------------------------------------------------

// Adresse du back
const server = 'http://localhost:3000/'

// DOM
const calendar = document.getElementById('calendar')
const reservationsDiv = document.getElementById('reservations')

// Calendar
const months = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Decembre']
const daysOfWeek = ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam']
let state = { month: new Date().getMonth(), year: new Date().getFullYear() }

// ----------------------------------------------------------------------------
//      FONCTIONS
// ----------------------------------------------------------------------------

function datesForGrid(year, month) {
    let dates = []
    const gridsize = 42

    let today = new Date()
    let firstDay = new Date(year, month).getDay()
    let totalDaysInMonth = new Date(year, month + 1, 0).getDate()
    let totalDaysInPrevMonth = new Date(year, month, 0).getDate()

    // Jours du mois précédent
    for (let i = 1; i <= firstDay; i++) {
        let prevMonthDate = totalDaysInPrevMonth - firstDay + i;
        let key = new Date(state.year, state.month - 1, prevMonthDate).toLocaleString()
        dates.push({ key: key, date: prevMonthDate, monthClass: 'prev' })
    }

    // Jours du mois en cours
    for (let i = 1; i <= totalDaysInMonth; i++) {
        let key = new Date(state.year, state.month, i).toLocaleString();
        if (i === today.getDate() && state.month === today.getMonth() && state.year === today.getFullYear()) {
            dates.push({ key: key, date: i, monthClass: 'current', todayClass: 'today' });
        } else {
            dates.push({ key: key, date: i, monthClass: 'current' });
        }
    }

    // Jours du mois suivant
    if (dates.length < gridsize) {
        let count = gridsize - dates.length;
        for (let i = 1; i <= count; i++) {
            let key = new Date(state.year, state.month + 1, i).toLocaleString();
            dates.push({ key: key, date: i, monthClass: 'next' });
        }
    }

    return dates
}

function render() {
    calendar.innerHTML = `
      <div class="calendar-nav">
        <button id="prev-month">Précédent</button>
        <h3>${months[state.month]} ${state.year}</h3>
        <button id="next-month">Suivant</button>
      </div>
      <div class='calendar-grid'>
        ${daysOfWeek.map(day => `<div class="legend">${day}</div>`).join('')}
        ${datesForGrid(state.year, state.month).map(date => `<div id="${date.key}" class="day ${date.monthClass} ${date.todayClass ? date.todayClass : ''}" onload="checkReservations('${date.key}')" onclick="showReservations('${date.key}')">${date.date}</div>`).join('')}
      </div>
    `;
}

function showCalendar(prevNextIndicator) {
    var date = new Date(state.year, state.month + prevNextIndicator);
    //Update the state
    state.year = date.getFullYear();
    state.month = date.getMonth();
    render();

    let days = document.getElementsByClassName('day')
    for (let day of days) {
        checkReservations(day.id)
    }
}

// Show the current month by default
showCalendar(0);

document.addEventListener('click', function (ev) {
    if (ev.target.id === 'prev-month') {
        showCalendar(-1);
    }
    if (ev.target.id === 'next-month') {
        showCalendar(1);
    }
});

async function checkReservations(data) {
    let date = new Date(data)
    let schedule = await getSchedule()

    let year = date.getFullYear()
    let month = date.getMonth()
    let day = date.getDate()

    if (schedule.reserved[year] && schedule.reserved[year][month] && schedule.reserved[year][month][day]) {
        document.getElementById(data).classList.add('notif')
    }
}

async function showReservations(data) {
    highlight(data)

    let date = new Date(data)
    let schedule = await getSchedule()

    let year = date.getFullYear()
    let month = date.getMonth()
    let day = date.getDate()

    let reservations = {}

    if (schedule.reserved[year][month][day]) {
        reservations = schedule.reserved[year][month][day]
    }

    reservationsDiv.innerHTML = ""

    for (const [hour, customer] of Object.entries(reservations)) {
        addReservation(reservationsDiv, hour, customer, data)
    }
}

function highlight(data) {
    let oldHighlight = document.querySelector('.highlight')
    if (oldHighlight != undefined) oldHighlight.classList.remove('highlight')

    let newHighlight = document.getElementById(`${data}`)
    newHighlight.classList.add('highlight')
}

async function getSchedule() {
    return await fetch(server).then((r) => r.json())
}

function addReservation(parent, hourRes, customer, date) {
    let res = document.createElement('div')
    let hour = document.createElement('p')
    let name = document.createElement('p')
    let tel = document.createElement('a')
    let mail = document.createElement('a')
    let close = document.createElement('p')

    hour.innerText = `${hourRes}H`
    name.innerText = `${customer.firstname.charAt(0).toUpperCase() + customer.firstname.slice(1)} ${customer.name.toUpperCase()}`
    tel.innerText = customer.tel
    tel.href = `tel:${customer.tel}`
    mail.innerText = customer.mail
    mail.href = `mailto:${customer.mail}`
    close.innerText = "Supprimer ✖"

    res.classList.add('customer-reservation')
    close.addEventListener('click', (e) => deleteReservation(e, date, hourRes))

    res.appendChild(hour)
    res.appendChild(name)
    res.appendChild(tel)
    res.appendChild(mail)
    res.appendChild(close)

    parent.appendChild(res)
}

function deleteReservation(e, date, hour) {
    date = new Date(date)
    let parent = e.currentTarget.parentNode

    let toSend = {
        year: date.getFullYear(),
        month: date.getMonth(),
        day: date.getDate(),
        hour: hour
    }

    fetch(`${server}delete`, {

        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(toSend)

    }).then((res) => {

        if (res.ok) parent.parentNode.removeChild(parent)

    }).catch((err) => {
        console.log(err)
    })
}