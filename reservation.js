// ----------------------------------------------------------------------------
//      CONFIG
// ----------------------------------------------------------------------------

// Adresse du back
const server = 'http://localhost:3000/'

// Nombre de jours à afficher (à partir du lendemain)
const daysToDisplay = 7

// Date actuelle
const actualDate = new Date()

// DOM
const calendar = document.getElementById('calendar')
const formWrapper = document.querySelector('.formWrapper')
const form = document.querySelector('.popupForm')
const closeBtn = document.querySelector('.closeButton')

// Events
form.addEventListener('submit', submitForm)
closeBtn.addEventListener('click', closePopup)

// Reservation
let currentSlot = { year: "", month: "", day: "", hour: "", client: {} }

// ----------------------------------------------------------------------------
//      FONCTIONS
// ----------------------------------------------------------------------------

async function getSchedule() {
    return await fetch(server).then((r) => r.json())
}

function getFreeSlot(schedule, day) {
    let year = day.getFullYear()
    let month = day.getMonth()
    let monthday = day.getDate()
    let weekday = day.getDay()

    let daySchedule = schedule.list[weekday]

    let freeSlots = []

    if (daySchedule.length == 2) {
        for (let i = daySchedule[0]; i < daySchedule[1]; i++) {
            if (!schedule.reserved[year]?.[month]?.[monthday]?.[i]) freeSlots.push(i)
        }
    }

    if (daySchedule.length == 4) {
        for (let i = daySchedule[0]; i < daySchedule[1]; i++) {
            if (!schedule.reserved[year]?.[month]?.[monthday]?.[i]) freeSlots.push(i)
        }
        for (let i = daySchedule[2]; i < daySchedule[3]; i++) {
            if (!schedule.reserved[year]?.[month]?.[monthday]?.[i]) freeSlots.push(i)
        }
    }

    return freeSlots
}

function createSlot(parent, hour, date) {
    let slotToAdd = document.createElement('a')
    slotToAdd.classList.add('slot')

    slotToAdd.innerText = `${hour}H`

    parent.appendChild(slotToAdd)

    slotToAdd.addEventListener('click', () => {
        currentSlot.year = date.getFullYear()
        currentSlot.month = date.getMonth()
        currentSlot.day = date.getDate()
        currentSlot.hour = hour

        formWrapper.style.display = "flex"
    })
}

function createDay(parent, date, freeSlots) {
    let dayToAdd = document.createElement('details')
    let title = document.createElement('summary')
    let slots = document.createElement('div')

    const options = { weekday: 'long', day: 'numeric', month: 'long' }
    let dateString = date.toLocaleDateString('fr-FR', options)
    // Ajouter une majuscule à la première lettre
    dateString = dateString.charAt(0).toUpperCase() + dateString.slice(1)
    title.innerText = dateString
    dayToAdd.appendChild(title)

    slots.classList.add('slotsList')
    dayToAdd.appendChild(slots)

    freeSlots.forEach((slot) => createSlot(slots, slot, date))

    parent.appendChild(dayToAdd)
}

function closePopup() {
    formWrapper.style.display = "none"
}

function submitMessage(status) {
    let msg = document.createElement('p')
    msg.classList.add('submitMessage')

    if (status) {
        msg.innerText = "Votre réservation à bien été validée."
        msg.classList.add('validated')
    } else {
        msg.innerText = "Une erreur est survenue, veuillez réessayer ultérieurement."
        msg.classList.add('error')
    }

    document.body.appendChild(msg)

    form.reset()

    setTimeout(() => document.body.removeChild(msg), 2800)
}

async function submitForm(event) {
    event.preventDefault()

    currentSlot.client = {
        firstname: document.getElementById('firstname').value,
        name: document.getElementById('name').value,
        tel: document.getElementById('tel').value,
        mail: document.getElementById('email').value
    }

    fetch(server, {

        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(currentSlot)

    }).then((res) => {

        formWrapper.style.display = "none"
        setTimeout(() => submitMessage(res.ok), 600)

    }).catch((err) => {

        setTimeout(() => submitMessage(false), 600)

    })
}

// ----------------------------------------------------------------------------
//      MAIN
// ----------------------------------------------------------------------------

async function main() {
    const schedule = await getSchedule()

    for (let i = 1; i <= daysToDisplay; i++) {
        // Copie la date du jour actuel (pas la référence)
        let day = new Date(actualDate.getTime())

        day.setDate(day.getDate() + i)

        if (schedule.list[day.getDay()].length > 0) {
            let freeSlots = getFreeSlot(schedule, day)
            if (freeSlots.length > 0) createDay(calendar, day, freeSlots)
        }
    }
}

main()